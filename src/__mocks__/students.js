import { v4 as uuid } from 'uuid';

export default [
  {
    id: uuid(),
    username: 'ekaterina.tankova',
    fullname: 'Ekaterina Tankova',
    phone: '03044283097',
    classcode: '11-SAY1'
  },
  {
    id: uuid(),
    username: 'cao.yu',
    fullname: 'Cao Yu',
    phone: '07123515711',
    classcode: '12-SAY3'
  },
  {
    id: uuid(),
    username: 'alexa.richardson',
    fullname: 'Alexa Richardson',
    phone: '07706352682',
    classcode: '10-SÖZ1'
  },
  {
    id: uuid(),
    username: 'anje.keizer',
    fullname: 'Anje Keizer',
    phone: '09086913242',
    classcode: '9-SAY1'
  },
  {
    id: uuid(),
    username: 'clarke.gillebert',
    fullname: 'Clarke Gillebert',
    phone: '09723334106',
    classcode: '11-EA1'
  },
  {
    id: uuid(),
    username: 'adam.denisov',
    fullname: 'Adam Denisov',
    phone: '08586023409',
    classcode: '10-EA4'
  },
  {
    id: uuid(),
    username: 'ava.gregoraci',
    fullname: 'Ava Gregoraci',
    phone: '04159072647',
    classcode: '11-SAY1'
  },
  {
    id: uuid(),
    username: 'emilee.simchenko',
    fullname: 'Emilee Simchenko',
    phone: '07026611654',
    classcode: '12-SAY1'
  },
  {
    id: uuid(),
    username: 'kwak.seong.min',
    fullname: 'Kwak SeongMin',
    phone: '03138128947',
    classcode: '11-EA1'
  },
  {
    id: uuid(),
    username: 'merrile.burgett',
    fullname: 'Merrile Burgett',
    phone: '08013017894',
    classcode: '12-SÖZ1'
  }
];
