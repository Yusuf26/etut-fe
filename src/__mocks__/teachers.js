import { v4 as uuid } from 'uuid';

export default [
  {
    id: uuid(),
    username: 'ekaterina.tankova',
    fullname: 'Ekaterina Tankova',
    phone: '03044283097',
    major: 'Matematik'
  },
  {
    id: uuid(),
    username: 'cao.yu',
    fullname: 'Cao Yu',
    phone: '07123515711',
    major: 'Kimya'
  },
  {
    id: uuid(),
    username: 'alexa.richardson',
    fullname: 'Alexa Richardson',
    phone: '07706352682',
    major: 'Fizik'
  },
  {
    id: uuid(),
    username: 'anje.keizer',
    fullname: 'Anje Keizer',
    phone: '09086913242',
    major: 'Matematik'
  },
  {
    id: uuid(),
    username: 'clarke.gillebert',
    fullname: 'Clarke Gillebert',
    phone: '09723334106',
    major: 'Türkçe'
  },
  {
    id: uuid(),
    username: 'adam.denisov',
    fullname: 'Adam Denisov',
    phone: '08586023409',
    major: 'Tarih'
  },
  {
    id: uuid(),
    username: 'ava.gregoraci',
    fullname: 'Ava Gregoraci',
    phone: '04159072647',
    major: 'Matematik'
  },
  {
    id: uuid(),
    username: 'emilee.simchenko',
    fullname: 'Emilee Simchenko',
    phone: '07026611654',
    major: 'Coğrafya'
  },
  {
    id: uuid(),
    username: 'kwak.seong.min',
    fullname: 'Kwak SeongMin',
    phone: '03138128947',
    major: 'Fizik'
  },
  {
    id: uuid(),
    username: 'merrile.burgett',
    fullname: 'Merrile Burgett',
    phone: '08013017894',
    major: 'Matematik'
  }
];
